FROM node:lts-alpine as builder
ENV NODE_ENV production
# override the value set for Gitlab Pages
ENV PUBLIC_URL / 
USER node
RUN mkdir -p /home/node/app
WORKDIR /home/node/app
COPY ./package.json ./
RUN npm install --omit=dev
COPY . .
RUN npm run build

FROM nginx:stable-alpine as production
COPY --from=builder /home/node/app/build /usr/share/nginx/html
COPY --from=builder /home/node/app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
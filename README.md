# Tetris

<div align="center">
<img src="tetris.png" width="700">
</div>

A remake of the classic from 1984: Tetris. It was built with React to practice using Hooks and Styled Components.

**[Play Now!](https://asuar.gitlab.io/react-tetris/)**

## Features :star2:

:star: Responsive UI design\
:star: Background Animations\
:star: Button and keyboard controls\
:star: Next piece display\
:star: Shadow indicator for better piece placement

## Technologies used 🛠️

- [React](https://es.reactjs.org/) - Front-End JavaScript library
- [Styled Components](https://github.com/styled-components/styled-components) - Library that allows using CSS in React component style
- [React Bootstrap](https://react-bootstrap.github.io/) - Front-End UI library rebuilt for React

## Try out my code 🚀

This code can be deployed directly on a service such as [Netlify](https://netlify.com). Here is how to install it locally:

### Prerequisites :clipboard:

[Git](https://git-scm.com)\
[Docker](https://www.docker.com/)

Without Docker add:\
[NPM](http://npmjs.com)\
[React](https://es.reactjs.org/)

### Setup :wrench:

From the command line, first clone react-tetris:

```bash
# Clone this repository
$ git clone https://gitlab.com/asuar/react-tetris.git

# Go into the repository
$ cd react-tetris

# Remove current origin repository
$ git remote remove origin
```

### Install with Docker

```bash
# Use docker compose to run app in development mode
$ docker-compose -f docker-compose.dev.yml up

# or use an NPM shortcut
$ npm run dev
```

### Install with NPM

```bash
# Install dependencies
$ npm install

# Start development server
$ npm start
```

Once the server has started, the site can be accessed at `http://localhost:3000/`

### Deploy with Docker

The app is setup to deploy on Gitlab Pages but can be deployed with Docker using nginx:

```bash
# Use docker compose to run app for production
$ docker-compose up --build

# or use an NPM shortcut
$ npm run prod
```

## License 📄

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

const size = {
  sm: '576px',
  md: '768px',
  lg: '1200px',
  xl: '1400px',
};
const device = {
  sm: `(min-width: ${size.sm})`,
  md: `(min-width: ${size.md})`,
  lg: `(min-width: ${size.lg})`,
  xl: `(min-width: ${size.xl})`,
};
export default { size, device };

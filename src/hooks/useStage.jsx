import { useState, useEffect } from 'react';
import { createStage } from '../utils/gameHelpers';

// take player and stage draw the player
const drawPlayerPiece = (player, stage) => {
  const playerStage = stage;
  player.tetromino.forEach((row, y) => {
    row.forEach((value, x) => {
      if (value !== 0) {
        playerStage[y + player.pos.y][x + player.pos.x] = [
          value,
          `${player.collided ? 'merged' : 'clear'}`,
        ];
      }
    });
  });
  return playerStage;
};

const drawShadowPlayer = (shadowPlayer, stage) => {
  const shadowStage = stage;
  if (typeof shadowPlayer !== 'undefined') {
    shadowPlayer.tetromino.forEach((row, y) => {
      row.forEach((value, x) => {
        if (value !== 0) {
          shadowStage[y + shadowPlayer.pos.y][x + shadowPlayer.pos.x] = [
            'shadow',
            'clear',
          ];
        }
      });
    });
  }
  return shadowStage;
};

const drawPieces = (player, stage) => {
  let newStage = stage;
  // if order is changed shadow will block player collision
  newStage = drawShadowPlayer(player.shadowPlayer, stage);
  newStage = drawPlayerPiece(player, newStage);
  return newStage;
};

export const useStage = (player) => {
  const [stage, setStage] = useState(createStage());
  const [rowsCleared, setRowsCleared] = useState(0);

  // reset stage back to blank
  const resetStage = () => {
    setStage(createStage());
  };

  // clear row
  const sweepRows = (sweepStage) => sweepStage.reduce((ack, row) => {
    if (row.findIndex((cell) => cell[0] === 0) === -1) {
      setRowsCleared((prev) => prev + 1);
      ack.unshift(new Array(sweepStage[0].length).fill([0, 'clear']));
      return ack;
    }
    ack.push(row);
    return ack;
  }, []);

  useEffect(() => {
    // update stage based on previous state
    const updateStage = (prevStage) => {
      let newStage = prevStage.map((row) => row.map((cell) => (cell[1] === 'clear' ? [0, 'clear'] : cell)));

      newStage = drawPieces(player, newStage);
      if (player.collided) return sweepRows(newStage);

      return newStage;
    };

    // move stage one tick
    const advanceStage = () => {
      setStage((prev) => updateStage(prev));
    };

    setRowsCleared(0);
    advanceStage();
  }, [player]);

  return [stage, rowsCleared, resetStage];
};

export default useStage;

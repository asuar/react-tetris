import { useState, useCallback, useEffect } from 'react';
import { STAGE_WIDTH, checkCollision } from '../utils/gameHelpers';
import { TETROMINOS, randomTetromino } from '../utils/tetrominos';

export const usePlayer = () => {
  const [player, setPlayer] = useState({
    pos: { x: 0, y: 0 },
    tetromino: TETROMINOS[0].shape,
    nextTetromino: randomTetromino().shape,
    collided: false,
    shadowPlayer: { pos: { x: 0, y: 0 }, tetromino: TETROMINOS[0].shape },
  });

  const rotate = (matrix, dir) => {
    // Make the rows into cols (transpose)
    const rotatedTetro = matrix.map((_, index) => matrix.map((col) => col[index]));
    // Reverse each row to get a rotated matrix
    if (dir > 0) return rotatedTetro.map((row) => row.reverse());
    return rotatedTetro.reverse();
  };

  const getShadowPlayer = (stage, ControllerPlayer) => {
    const clonedPlayer = JSON.parse(JSON.stringify(ControllerPlayer));
    while (!checkCollision(clonedPlayer, stage, { x: 0, y: 1 })) {
      clonedPlayer.pos.y += 1;
    }
    return { pos: clonedPlayer.pos, tetromino: clonedPlayer.tetromino };
  };

  const playerRotate = (stage, dir) => {
    const clonedPlayer = JSON.parse(JSON.stringify(player));
    clonedPlayer.tetromino = rotate(clonedPlayer.tetromino, dir);

    const pos = clonedPlayer.pos.x;
    let offset = 1;
    while (checkCollision(clonedPlayer, stage, { x: 0, y: 0 })) {
      clonedPlayer.pos.x += offset;
      offset = -(offset + (offset > 0 ? 1 : -1));
      if (offset > clonedPlayer.tetromino[0].length) {
        rotate(clonedPlayer.tetromino, -dir);
        clonedPlayer.pos.x = pos;
        return;
      }
    }
    clonedPlayer.shadowPlayer = getShadowPlayer(stage, clonedPlayer);
    setPlayer(clonedPlayer);
  };

  const dropPlayer = (stage) => {
    const clonedPlayer = JSON.parse(JSON.stringify(player));
    while (!checkCollision(clonedPlayer, stage, { x: 0, y: 1 })) {
      clonedPlayer.pos.y += 1;
    }
    clonedPlayer.collided = true;
    clonedPlayer.shadowPlayer = getShadowPlayer(stage, clonedPlayer);
    setPlayer(clonedPlayer);
  };

  const updatePlayerPos = ({ x, y, collided }, stage) => {
    const shadowPlayer = getShadowPlayer(stage, {
      pos: { x: player.pos.x + x, y: player.pos.y + y },
      tetromino: player.tetromino,
    });

    setPlayer((prev) => ({
      ...prev,
      pos: { x: prev.pos.x + x, y: prev.pos.y + y },
      collided,
      shadowPlayer,
    }));
  };

  const movePlayer = (x, y, stage) => {
    if (!checkCollision(player, stage, { x, y })) {
      updatePlayerPos(
        {
          x,
          y,
          collided: false,
        },
        stage,
      );
      return true;
    }
    if (y > 0) {
      updatePlayerPos(
        {
          x: 0,
          y: 0,
          collided: true,
        },
        stage,
      );
      return false;
    }
    return true;
  };

  const resetPlayer = useCallback(() => {
    setPlayer({
      pos: { x: STAGE_WIDTH / 2 - 2, y: 0 },
      tetromino: player.nextTetromino,
      nextTetromino: randomTetromino().shape,
      collided: false,
      shadowPlayer: { pos: { x: 0, y: 0 }, tetromino: TETROMINOS[0].shape },
    });
  }, [player.nextTetromino]);

  const initPlayer = () => {
    resetPlayer();
  };

  useEffect(() => {
    if (player.collided) resetPlayer();
  }, [player.collided, resetPlayer]);

  return [player, initPlayer, playerRotate, movePlayer, dropPlayer];
};

export default usePlayer;

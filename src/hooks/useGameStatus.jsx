import { useState, useEffect, useCallback } from 'react';

export const useGameStatus = (rowsCleared) => {
  const [score, setScore] = useState(0);
  const [rows, setRows] = useState(0);
  const [level, setLevel] = useState(0);

  const resetGameStatus = () => {
    setRows(0);
    setScore(0);
    setLevel(0);
  };

  const calcScore = useCallback(() => {
    const linePoints = [40, 100, 300, 1200];
    // We have score
    if (rowsCleared > 0) {
      // Based on original Tetris scoring
      setScore((prev) => prev + linePoints[rowsCleared - 1] * (level + 1));
      setRows((prev) => prev + rowsCleared);

      // Increase level when player has cleared 10 rows
      if (rows > (level + 1) * 10) {
        setLevel(level + 1);
      }
    }
  }, [level, rows, rowsCleared]);

  useEffect(() => {
    calcScore();
  }, [calcScore, rowsCleared, score]);

  return [score, rows, level, resetGameStatus];
};

export default useGameStatus;

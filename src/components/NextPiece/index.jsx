import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { StyledNextPiece, StyledPieceGrid } from './style';
import Cell from '../Cell';
import { createStage } from '../../utils/gameHelpers';

const resetStage = () => createStage(3, 3);

function NextPiece({ tetromino, gameStarted }) {
  const [stage, setStage] = useState(resetStage());

  const displayTetrimino = useCallback(
    (tempStage) => {
      const newStage = tempStage;
      tetromino.forEach((row, y) => {
        row.forEach((value, x) => {
          if (value !== 0 && y < tempStage.length && x < tempStage.length) {
            newStage[y][x] = [value, value];
          }
        });
      });
      return newStage;
    },
    [tetromino],
  );

  useEffect(() => {
    let newStage = resetStage();
    if (gameStarted) newStage = displayTetrimino(newStage);
    setStage(newStage);
  }, [tetromino, gameStarted, displayTetrimino]);

  return (
    <StyledNextPiece>
      NEXT
      <StyledPieceGrid>
        {stage.map((row) => row.map((cell, x) => (
          /* eslint-disable-next-line react/no-array-index-key */
          <Cell key={`${x}:${cell}`} type={cell[0]} />
        )))}
      </StyledPieceGrid>
    </StyledNextPiece>
  );
}

NextPiece.propTypes = {
  tetromino: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  ).isRequired,
  gameStarted: PropTypes.bool.isRequired,
};

export default NextPiece;

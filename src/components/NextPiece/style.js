import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints';

export const StyledNextPiece = styled.div`
  color: white;
  font-family: Pixel, Arial, Helvetica, sans-serif;
  font-size: 0.8rem;

  @media screen and (min-width: 540px) and (min-height: 720px) {
    font-size: 1rem;
  }

  @media screen and (min-width: 1024px) and (min-height: 600px) {
    font-size: 1rem;
  }

  @media screen and ${breakpoints.device.lg} {
    font-size: 1.5rem;
  }
`;

export const StyledPieceGrid = styled.div`
  display: grid;
  aspect-ratio: 1;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: 1fr;
  grid-gap: 0px;
  background: rgb(40 40 40);
  border: 3px solid #333;
  width: 100%;

  @media screen and (min-width: 540px) and (min-height: 720px) {
    width: 90%;
  }

  @media screen and (min-width: 1024px) and (min-height: 600px) {
    width: 70%;
  }

  @media screen and ${breakpoints.device.lg} {
    width: 100%;
  }
`;

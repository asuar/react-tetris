import React from 'react';
import PropTypes from 'prop-types';
import { StyledCell } from './style';
import { TETROMINOS } from '../../utils/tetrominos';

function Cell({ type }) {
  return <StyledCell type={type} color={TETROMINOS[type].color} />;
}

Cell.propTypes = {
  type: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

export default React.memo(Cell);

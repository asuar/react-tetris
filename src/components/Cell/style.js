import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints';

export const StyledCell = styled.div`
  background: rgba(${(props) => props.color}, 0.8);
  border: ${(props) => (props.type === 0 ? '0px solid' : '1px solid')};
  border-bottom-color: rgba(${(props) => props.color}, 0.1);
  border-right-color: rgba(${(props) => props.color}, 1);
  border-top-color: rgba(${(props) => props.color}, 1);
  border-left-color: rgba(${(props) => props.color}, 0.3);

  @media screen and ${breakpoints.device.sm} {
    border: ${(props) => (props.type === 0 ? '0px solid' : '4px solid')};
    border-bottom-color: rgba(${(props) => props.color}, 0.1);
    border-right-color: rgba(${(props) => props.color}, 1);
    border-top-color: rgba(${(props) => props.color}, 1);
    border-left-color: rgba(${(props) => props.color}, 0.3);
  }
`;

export default StyledCell;

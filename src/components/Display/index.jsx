import React from 'react';
import PropTypes from 'prop-types';
import { StyledDisplay } from './style';

function Display({ gameOver = false, text }) {
  return <StyledDisplay gameOver={gameOver}>{text}</StyledDisplay>;
}

Display.defaultProps = {
  gameOver: false,
};

Display.propTypes = {
  gameOver: PropTypes.bool,
  text: PropTypes.string.isRequired,
};

export default Display;

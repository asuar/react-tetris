import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints';

export const StyledDisplay = styled.div`
  text-align: start;
  box-sizing: border-box;
  width: 100%;
  border-radius: 1rem;
  color: ${(props) => (props.gameOver ? 'red' : 'white')};
  text-align: ${(props) => (props.gameOver ? 'center' : 'start')}
  background: #000;
  font-family: Pixel, Arial, Helvetica, sans-serif;
  user-select: none;
  border: 2px solid #333;
  padding: 0.25rem;
  margin: 0 0 0.25rem 0;
  font-size: 0.5rem;

  @media screen and ${breakpoints.device.md} {
    margin: 0 0 0.5rem 0;
    border: 4px solid #333;
    font-size: 0.8rem;
    padding: 0.5rem;
    min-height: 2rem;
  }

  @media screen and (min-width: 1024px) and (min-height: 600px) {
    margin: 0 0 0.25rem 0;
    border: 2px solid #333;
    font-size: 0.5rem;
    padding: 0.25rem;
  }

  @media screen and (min-width: 1024px) and (min-height: 1366px) {
    margin: 0 0 0.5rem 0;
    border: 4px solid #333;
    font-size: 0.8rem;
    padding: 0.5rem;
  }

  @media screen and ${breakpoints.device.lg} {
    margin: 0 0 0.5rem 0;
    border: 4px solid #333;
    font-size: 0.8rem;
    padding: 1rem;
    min-height: 2rem;
    text-align: start;
  }
`;

export default StyledDisplay;

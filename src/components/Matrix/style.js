import styled from 'styled-components';

export const StyledMatrix = styled.canvas`
  width: 100vw;
  height: 100vh;
  background: #000
  z-index: -1;
  display: block;
  position: fixed;
  top: 0;
  left: 0;
`;

export default StyledMatrix;

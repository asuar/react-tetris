import React from 'react';
import { StyledMatrix } from './style';

const FONT_SIZE = 9;

const drawMatrix = (context, w, h, ypos) => {
  const Ypositions = ypos;
  // Draw a semitransparent black rectangle on top of previous drawing
  context.fillStyle = '#0001';
  context.fillRect(0, 0, w, h);

  context.fillStyle = '#0f0';
  context.font = `${FONT_SIZE}pt monospace`;

  // for each column put a random character at the end
  Ypositions.forEach((y, ind) => {
    // generate a random character
    const text = String.fromCharCode(
      Math.random() * (Math.ceil(128) - Math.floor(32)) + Math.floor(32),
    );

    // x of the column, y is already given
    const x = ind * (FONT_SIZE + 4);
    context.fillText(text, x, y);

    // randomly reset the end of the column
    if (y > window.innerHeight) Ypositions[ind] = Math.random() * 800;
    // otherwise just move the y for the column 20px down,
    else Ypositions[ind] = y + (FONT_SIZE + 5);
  });
};

export function setupMatrix(prevypos) {
  const canvas = document.getElementById('matrix');
  const context = canvas.getContext('2d');
  canvas.width = document.body.offsetWidth;
  const w = canvas.width;
  canvas.height = document.body.offsetHeight;
  const h = canvas.height;
  context.fillStyle = '#000';
  context.fillRect(0, 0, w, h);

  const cols = Math.floor(w / (FONT_SIZE + 4)) + 1;
  const ypos = prevypos === undefined ? Array(cols).fill(0) : prevypos;

  // render at 20ish FPS
  return [setInterval(drawMatrix, 50, context, w, h, ypos), ypos];
}

function Matrix() {
  return <StyledMatrix id="matrix" />;
}

export default Matrix;

import React from 'react';
import PropTypes from 'prop-types';

// React-bootstrap
import { Col } from 'react-bootstrap';

import GameButton from '../GameButton';
import { MenuButtonWrapper, UIWrapper } from './style';
import GameInfo from './GameInfo';
import Display from '../Display';

function Menu({
  gameInfo,
  gameOver = false,
  startGame,
  gameStarted,
  togglePauseGame,
  gamePaused,
  nextPiece,
}) {
  const menu = (
    <MenuButtonWrapper>
      {!gameStarted && !gamePaused && !gameOver && (
        <GameButton callback={startGame} label="Play" />
      )}
      {gameStarted && !gamePaused && !gameOver && (
        <GameButton callback={togglePauseGame} label="Pause" />
      )}
      {gamePaused && (
        <>
          <GameButton callback={startGame} label="Restart" />
          <GameButton callback={togglePauseGame} label="Resume" />
        </>
      )}
      {gameOver && (
        <>
          <Display gameOver={gameOver} text="GAME OVER" />
          <GameButton callback={startGame} label="Play Again" />
        </>
      )}
    </MenuButtonWrapper>
  );

  return (
    <UIWrapper>
      <Col className="col-6 col-xl-6">
        <GameInfo
          level={gameInfo.level}
          score={gameInfo.score}
          rows={gameInfo.rows}
          gameStarted={gameStarted}
          nextPiece={nextPiece}
        />
      </Col>
      <Col className="col-6 col-xl-6">{menu}</Col>
    </UIWrapper>
  );
}

Menu.defaultProps = {
  gameOver: false,
};

Menu.propTypes = {
  gameInfo: PropTypes.shape({
    level: PropTypes.number.isRequired,
    score: PropTypes.number.isRequired,
    rows: PropTypes.number.isRequired,
  }).isRequired,
  gameOver: PropTypes.bool,
  startGame: PropTypes.func.isRequired,
  gameStarted: PropTypes.bool.isRequired,
  togglePauseGame: PropTypes.func.isRequired,
  gamePaused: PropTypes.bool.isRequired,
  nextPiece: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  ).isRequired,
};

export default Menu;

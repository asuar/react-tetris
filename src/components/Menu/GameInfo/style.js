import styled from 'styled-components';

export const MenuDisplayWrapper = styled.div`
  display: inline-block;
  width: 100%;
  padding: 0px;
`;

export default MenuDisplayWrapper;

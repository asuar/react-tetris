import React from 'react';
import PropTypes from 'prop-types';

// React-bootstrap
import { Col, Row } from 'react-bootstrap';

import Display from '../../Display';
import NextPiece from '../../NextPiece';

import { MenuDisplayWrapper } from './style';

function GameInfo({
  level, score, rows, gameStarted, nextPiece,
}) {
  return (
    <Row>
      <Col className="col-4 px-1 order-2 order-xl-1">
        <NextPiece tetromino={nextPiece} gameStarted={gameStarted} />
      </Col>
      <Col className="col-8 col-xl-12 px-1 order-1 order-xl-2">
        <MenuDisplayWrapper>
          <Display text={`Score: ${score}`} />
          <Display text={`Rows: ${rows}`} />
          <Display text={`Level: ${level}`} />
        </MenuDisplayWrapper>
      </Col>
    </Row>
  );
}

GameInfo.propTypes = {
  level: PropTypes.number.isRequired,
  score: PropTypes.number.isRequired,
  rows: PropTypes.number.isRequired,
  gameStarted: PropTypes.bool.isRequired,
  nextPiece: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  ).isRequired,
};

export default GameInfo;

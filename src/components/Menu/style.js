import styled from 'styled-components';
import { Row } from 'react-bootstrap';

export const MenuButtonWrapper = styled.div`
  display: inline-block;
  vertical-align: center;
  width: 100%;
`;

export const UIWrapper = styled(Row)`
  align-items: end;
`;

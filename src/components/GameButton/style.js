import styled from 'styled-components';
import breakpoints from '../../styles/breakpoints';
import { ControllerWrapper } from '../Controller/style';
import { MenuButtonWrapper } from '../Menu/style';

export const StyledGameButton = styled.button`
  box-sizing: border-box;
  padding: 0.75rem;
  border-radius: 2rem;
  border: none;
  color: white;
  background: #333;
  font-family: Pixel, Arial, Helvetica, sans-serif;
  font-size: 0.7rem;
  outline: none;
  cursor: pointer;
  line-height: 1.5;
  box-shadow: 0.1em 0.2em gray;
  user-select: none;

  @media screen and ${breakpoints.device.md} {
    font-size: 1rem;
    padding: 1rem;
  }

  @media screen and (min-width: 1024px) and (min-height: 600px) {
    font-size: 0.7rem;
    padding: 0.5rem;
  }

  @media screen and (min-width: 1024px) and (min-height: 1366px) {
    font-size: 1.5rem;
    padding: 1.5rem;
  }

  @media screen and ${breakpoints.device.lg} {
    font-size: 1rem;
    padding: 1rem;
  }

  ${ControllerWrapper} & {
    color: white;
    margin: 0 0 0 0;
    border-radius: 3rem;
    font-size: 1.5rem;
    line-height: 0.5;

    sub {
      font-size: 1rem;
      vertical-align: sub;
    }
  }

  ${MenuButtonWrapper} & {
    color: white;
    width: 100%;
    margin: 0 0 0.5rem 0;
    border-radius: 1rem;
    padding: 0.4rem;

    @media screen and ${breakpoints.device.sm} {
      margin: 0 0 1rem 0;
    }
  }

  :active {
    box-shadow: none;
    position: relative;
    top: 0.2em;
  }
`;

export default StyledGameButton;

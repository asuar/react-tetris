import React from 'react';
import PropTypes from 'prop-types';
import { StyledGameButton } from './style';

function GameButton({ callback, label }) {
  return (
    <StyledGameButton
      onClick={(event) => {
        event.target.blur(); // stop spacebar and enter from triggering this again
        return callback();
      }}
    >
      {label}
    </StyledGameButton>
  );
}

GameButton.propTypes = {
  callback: PropTypes.func.isRequired,
  label: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
};

export default GameButton;

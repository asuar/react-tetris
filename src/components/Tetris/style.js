import styled from 'styled-components';
import { Container } from 'react-bootstrap';
import breakpoints from '../../styles/breakpoints';

export const StyledTetrisWrapper = styled.div`
  height: 100%;
  :focus {
    outline: none;
  }
`;

export const StyledTetris = styled(Container)`
  margin: 0 auto;
  max-width: 75rem;
  padding-top: 1rem;

  .game-wrapper {
    height: 100%;
    align-items: center;
    justify-content: center;
  }

  .col {
    display: inline-block;
    margin-bottom: 0.25rem;
  }

  .stage-wrapper {
    max-width: 50vw;

    @media screen and (min-width: 375px) {
      max-width: 55vw;
    }

    @media screen and (min-width: 375px) and (min-height: 812px) {
      max-width: 100vw;
    }

    @media screen and (min-width: 414px) {
      max-width: 75vw;
    }

    @media screen and (min-width: 540px) and (min-height: 720px) {
      max-width: 58vw;
    }

    @media screen and ${breakpoints.device.md} {
      max-width: 55vw;
    }

    @media screen and (min-width: 1024px) and (min-height: 600px) {
      max-width: 21vw;
    }

    @media screen and (min-width: 1024px) and (min-height: 1366px) {
      max-width: 60vw;
    }

    @media screen and ${breakpoints.device.lg} {
      max-width: 30vw;
    }

    @media screen and ${breakpoints.device.xl} {
      max-width: 30vw;
    }
  }
`;

import React, { useState, useEffect, useCallback } from 'react';

// React-bootstrap
import { Row, Col } from 'react-bootstrap';

// Styled Components
import { StyledTetrisWrapper, StyledTetris } from './style';

// Custom Hooks
import { useInterval } from '../../hooks/useInterval';
import { usePlayer } from '../../hooks/usePlayer';
import { useStage } from '../../hooks/useStage';
import { useGameStatus } from '../../hooks/useGameStatus';
import { useWindowSize } from '../../hooks/useWindowSize';

// Componenets
import Stage from '../Stage';
import Menu from '../Menu';
import Controller from '../Controller';
import Matrix, { setupMatrix } from '../Matrix';

// utils
import { CONTROL_KEYS } from '../../utils/gameHelpers';

function Tetris() {
  const [dropTime, setDropTime] = useState(null);
  const [gameOver, setGameOver] = useState(false);
  const [matrixInfo, setMatrixInfo] = useState(null);
  const [gamePaused, setGamePause] = useState(false);
  const [gameStarted, setGameStart] = useState(false);
  const [prevScreenWidth, setPrevWidth] = useState(0);
  const [prevScreenHeight, setPrevHeight] = useState(0);

  const [player, initPlayer, playerRotate, movePlayer, dropPlayer] = usePlayer();
  const [stage, rowsCleared, resetStage] = useStage(player);
  const [score, rows, level, resetGameStatus] = useGameStatus(rowsCleared);
  const [screenWidth, screenHeight] = useWindowSize();

  const DROP_TIME = 1000;

  // redraw our matrix on window size change
  if (
    matrixInfo !== null
    && !gamePaused
    && (prevScreenWidth !== screenWidth || prevScreenHeight !== screenHeight)
  ) {
    setPrevWidth(screenWidth);
    setPrevHeight(screenHeight);
    clearInterval(matrixInfo[0]);
    setMatrixInfo(setupMatrix());
  }

  const startGame = () => {
    // Reset everything
    setGameStart(true);
    setGamePause(false);
    setGameOver(false);
    resetGameStatus();
    resetStage();
    initPlayer();
    setDropTime(DROP_TIME);

    if (matrixInfo !== null) clearInterval(matrixInfo[0]);
    setMatrixInfo(setupMatrix());
    document.getElementById('tetris-wrapper').focus();
  };

  const setNormalDropSpeed = useCallback(() => {
    setDropTime(DROP_TIME / (level + 1) + 200);
  }, [setDropTime, level]);

  const togglePauseGame = () => {
    if (gamePaused) {
      setGamePause(false);
      setMatrixInfo(setupMatrix(matrixInfo[1]));
      movePlayer(0, 1, stage); // move down 1 to stop stalling by pausing repeatedly
      setNormalDropSpeed();
    } else {
      setGamePause(true);
      setDropTime(null);
      clearInterval(matrixInfo[0]);
    }
    document.getElementById('tetris-wrapper').focus();
  };

  const move = (keyCode) => {
    if (!gameOver && !gamePaused && gameStarted) {
      if (keyCode === CONTROL_KEYS.LEFT_KEY) {
        movePlayer(-1, 0, stage);
      } else if (keyCode === CONTROL_KEYS.RIGHT_KEY) {
        movePlayer(1, 0, stage);
      } else if (keyCode === CONTROL_KEYS.DOWN_KEY) {
        dropPlayer(stage);
      } else if (keyCode === CONTROL_KEYS.ROTATE_COUNTER_CLOCKWISE_KEY) {
        playerRotate(stage, 1);
      } else if (keyCode === CONTROL_KEYS.ROTATE_CLOCKWISE_KEY) {
        playerRotate(stage, -1);
      }
      document.getElementById('tetris-wrapper').focus();
    }
  };

  useInterval(() => {
    movePlayer(0, 1, stage);
  }, dropTime);

  const managePlayerHit = useCallback(() => {
    // game over if collision at top
    if (player.pos.y <= 0) {
      setGameOver(true);
      setDropTime(null);
      clearInterval(matrixInfo[0]);
    } else {
      // update speed
      setNormalDropSpeed();
    }
  }, [player.pos.y, setNormalDropSpeed, matrixInfo]);

  useEffect(() => {
    if (player.collided) managePlayerHit();
  }, [player.collided, managePlayerHit]);

  return (
    <>
      <StyledTetrisWrapper
        tabIndex="0"
        onKeyDown={(event) => move(event.key)}
        id="tetris-wrapper"
      >
        <StyledTetris>
          <Row className="game-wrapper">
            <Col className="stage-wrapper col-12 col-xl-6">
              <Stage stage={stage} />
            </Col>
            <Col className="col-12 col-xl-6">
              <Menu
                gameInfo={{ level, score, rows }}
                gameOver={gameOver}
                startGame={startGame}
                gameStarted={gameStarted}
                togglePauseGame={togglePauseGame}
                gamePaused={gamePaused}
                nextPiece={player.nextTetromino}
              />
              <Row>
                <Col>
                  <Controller
                    className="col-12"
                    controlCallback={move}
                    controllerKeys={CONTROL_KEYS}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </StyledTetris>
      </StyledTetrisWrapper>
      <Matrix />
    </>
  );
}

export default Tetris;

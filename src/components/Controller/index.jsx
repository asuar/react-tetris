import React from 'react';
import PropTypes from 'prop-types';

// React-Bootstrap
import { Row, Col } from 'react-bootstrap';

// React-Icons
import {
  BsArrowLeft,
  BsArrowRight,
  BsArrowDown,
  BsArrowClockwise,
  BsArrowCounterclockwise,
} from 'react-icons/bs';

import GameButton from '../GameButton';
import { ControllerWrapper } from './style';

function Controller({ controlCallback, controllerKeys }) {
  return (
    <ControllerWrapper>
      <Row>
        <Col className="col-6">
          <GameButton
            callback={() => {
              controlCallback(controllerKeys.ROTATE_CLOCKWISE_KEY);
            }}
            label={(
              <>
                <BsArrowCounterclockwise />
                <sub>Q</sub>
              </>
            )}
          />
        </Col>
        <Col className="col-6">
          <GameButton
            callback={() => {
              controlCallback(controllerKeys.ROTATE_COUNTER_CLOCKWISE_KEY);
            }}
            label={(
              <>
                <BsArrowClockwise />
                <sub>E</sub>
              </>
            )}
          />
        </Col>
      </Row>
      <Row>
        <Col className="col-4 p-0">
          <GameButton
            callback={() => {
              controlCallback(controllerKeys.LEFT_KEY);
            }}
            label={(
              <>
                <BsArrowLeft />
                <sub>A</sub>
              </>
            )}
          />
        </Col>
        <Col className="col-4 p-0">
          <GameButton
            callback={() => {
              controlCallback(controllerKeys.DOWN_KEY);
            }}
            label={(
              <>
                <BsArrowDown />
                <sub>S</sub>
              </>
            )}
          />
        </Col>
        <Col className="col-4 p-0">
          <GameButton
            callback={() => {
              controlCallback(controllerKeys.RIGHT_KEY);
            }}
            label={(
              <>
                <BsArrowRight />
                <sub>D</sub>
              </>
            )}
          />
        </Col>
      </Row>
    </ControllerWrapper>
  );
}

Controller.propTypes = {
  controlCallback: PropTypes.func.isRequired,
  controllerKeys: PropTypes.shape({
    LEFT_KEY: PropTypes.string.isRequired,
    RIGHT_KEY: PropTypes.string.isRequired,
    DOWN_KEY: PropTypes.string.isRequired,
    ROTATE_CLOCKWISE_KEY: PropTypes.string.isRequired,
    ROTATE_COUNTER_CLOCKWISE_KEY: PropTypes.string.isRequired,
  }).isRequired,
};

export default Controller;

import styled from 'styled-components';
import { Container } from 'react-bootstrap';

export const ControllerWrapper = styled(Container)`
  .row {
    text-align: center;
  }

  .row:nth-child(2) {
    .col {
      margin-bottom: 0.5rem;
    }
  }
`;

export default ControllerWrapper;

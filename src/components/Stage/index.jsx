import React from 'react';
import PropTypes from 'prop-types';
import { StyledStage } from './style';

import Cell from '../Cell';

function Stage({ stage }) {
  return (
    <StyledStage width={stage[0].length} height={stage.length}>
      {stage.map((row) => row.map((cell, x) => (
        /* eslint-disable-next-line react/no-array-index-key */
        <Cell key={`${x}:${cell}`} type={cell[0]} />
      )))}
    </StyledStage>
  );
}

Stage.propTypes = {
  stage: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      ),
    ),
  ).isRequired,
};

export default Stage;

import styled from 'styled-components';

export const StyledStage = styled.div`
  display: grid;
  max-height: 90vh;
  aspect-ratio: 6/10;
  grid-template-columns: repeat(${(props) => props.width}, 1fr);
  grid-auto-rows: 1fr;
  grid-gap: 1px;
  border: 2px solid #333;
  background: rgb(40 40 40);
`;

export default StyledStage;

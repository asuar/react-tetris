export const STAGE_WIDTH = 12;
export const STAGE_HEIGHT = 20;

export const CONTROL_KEYS = {
  LEFT_KEY: 'a',
  RIGHT_KEY: 'd',
  DOWN_KEY: 's',
  ROTATE_CLOCKWISE_KEY: 'q',
  ROTATE_COUNTER_CLOCKWISE_KEY: 'e',
};
Object.freeze(CONTROL_KEYS);

// return true if we hit something
export const checkCollision = (player, stage, { x: moveX, y: moveY }) => {
  for (let y = 0; y < player.tetromino.length; y += 1) {
    for (let x = 0; x < player.tetromino[y].length; x += 1) {
      // Check that we're on an actual Tetromino cell
      if (player.tetromino[y][x] !== 0) {
        if (
          // Check that our move is inside the game area's height (y)
          !stage[y + player.pos.y + moveY]
          // Check that our move is inside the game areas width (x)
          || !stage[y + player.pos.y + moveY][x + player.pos.x + moveX]
          // Check that the cell we're moving to isn't set to clear
          || stage[y + player.pos.y + moveY][x + player.pos.x + moveX][1]
            !== 'clear'
        ) {
          return true;
        }
      }
    }
  }
  return false;
};

// create a blank stage
export const createStage = (height = STAGE_HEIGHT, width = STAGE_WIDTH) => Array.from(Array(height), () => new Array(width).fill([0, 'clear']));
